import './App.css';

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import AdminDashboard from './pages/AdminDashboard'
import Cart from './pages/Cart'
import MyOrders from './pages/MyOrders'
import ProductView from './components/ProductView';
import Footer from './components/Footer';

import { useState, useEffect } from 'react';

import {UserProvider} from "./UserContext";



import {Container} from 'react-bootstrap';

// Single Page Application (like facebook)
function App() {

  // State hook for the user state (Global Scope)

  // S55 - This will be used to store the user info and will be used for validating if a user is logged in on app or not
  //const [user, setUser]  = useState({email: localStorage.getItem('email')}); //S55

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  
  // use to check if the user info is properly stored upon login and the localStorage info is cleared upon logout

  useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(res => res.json())
      .then(data => {

          if(typeof data._id !== "undefined") {

              setUser({
                  id: data._id,
                  isAdmin: data.isAdmin
              })
          } 

          else { 
              setUser({
                  id: null,
                  isAdmin: null
              })
          }

      })
  }, []);



  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar />
      <Container>
        <Routes>
            
            <Route path="/" element={<Home />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />

            {(user.isAdmin === true) ?
            <>
            <Route path="/dashboard" element={<AdminDashboard />} />
            <Route path="/login" element={<Error />} />
            <Route path="/register" element={<Error />} />
            </>
            :
            <>

            <Route path="/products" element={<Products />} />
            <Route path="/products/:courseId" element={<ProductView />} />
            <Route path="/cart" element={<Cart />} />
            <Route path="/myorders" element={<MyOrders />} />
            <Route path="/login" element={<Error />} />
            <Route path="/register" element={<Error />} />
            </>
            }
            <Route path="*" element={<Error />}/>
          </Routes>
      </Container>
      <Footer />
    </Router>
    </UserProvider>
    </>
  );
}

export default App;
