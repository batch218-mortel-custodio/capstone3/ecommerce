import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Spinner } from 'react-bootstrap';
// import {useNavigate} from 'react-router-dom';
import {Navigate} from 'react-router-dom';

import Swal from 'sweetalert2';


import UserContext from '../UserContext';

export default function Login() {


    const {user, setUser} = useContext(UserContext);
    const [isLoading, setIsLoading] = useState(false);


	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

    // hook returns a function that lets us navigate to components
    // const navigate = useNavigate()

    // Function to simulate redirection via form submission
    const authenticate = async (e) => {
        // Prevents page redirection via form submission
        e.preventDefault();

        setIsLoading(true);

        await fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(typeof data.access !== "undefined") {
                localStorage.setItem('token', data.access);
                console.log("data.access", data.access);
                retrieveUserDetails(data.access);


                console.log("data.access", data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Le Burger!"
                })

                setIsLoading(false);
            } 
            else {
                    Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Please check your login details and try again."
                })

                setIsLoading(false);  

            };
        });

        // set the email if the authenticated user in the local storage.
        // localStorage.setItem('email', email);
        // sets the global user state to have properties obtained from local storage
        // setUser({email: localStorage.getItem('email')});

        // Clear input fields after submission
        setEmail('');
        setPassword('');
        // navigate('/');

        //alert(`${email} has been verified! Welcome back!`);

    };
//// this function below is not functioning
    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation accross the whole application
            setUser({
                id: data._id,
                firstName: data.firstName,
                isAdmin: data.isAdmin,
                email: data.email
            })

           
        })
    };



useEffect(() => {
    // Validation to enable submit button when all fields are populated and both passwords match
    if(email !== '' && password !== ''){
        setIsActive(true);
    }else{
        setIsActive(false);
    }
}, [email, password]);


return (

    (user.id !== null) ?
        (user.isAdmin === false) ?
            <Navigate to ="/products" />
            :
            <Navigate to ="/dashboard" />
        
        :
        <Row>
        <Col xs={2} md={4}/>
        <Col xs={8} md={4} className="pt-5">
            <Form onSubmit={(e) => authenticate(e)}>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email"
                        value={email}
                		onChange={(e) => setEmail(e.target.value)} 
                        required
                    />
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password" 
                        value={password}
                		onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>

                 { isActive ? 
                 <>
                <Button variant="primary" type="submit" id="submitBtn">
                    {isLoading ?         
                    <Spinner
                      as="span"
                      animation="border"
                      size="sm"
                      role="status"
                      aria-hidden="true"
                    />
                    : "Submit"}
                </Button>
                </>
                : 
                <>
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                Submit
                </Button>
                </>
                 }
            </Form>
        </Col>
        </Row>
    )
}













/*
import {useState, useEffect, useContext} from 'react';
import { Form, Button } from 'react-bootstrap';
//import {useNavigate} from 'react-router-dom';
import {Navigate} from 'react-router-dom';

import UserContext from "../UserContext";

import Swal from 'sweetalert2';

export default function Login() {


	// Allow us to use the User Context Object and its properties to be used for user validation -s54
	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState(false);


	// const navigate = useNavigate();

	function registerUser(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(typeof data.access !==  "undefined"){
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);
				
				Swal.fire({
				    title: "Login Successful",
				    icon: "success",
				    text: "Welcome to Zuitt!"
				})
			} 
			else {
				Swal.fire({
				 	title: "Authentication Failed",
				    icon: "error",
				    text: "Please check your login details and try again."
				})
			}
		});

		//localStorage.setItem('email', email); //S54

		// s54 - sets the global user state to have properties obtained from local storage
		// setUser({email: localStorage.getItem('email')});

		setEmail("");
		setPassword("");
		// navigate('/');
		
		//alert("Login successful!");

	}

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			// Changes the global "user" state to store the "id" and "isAdmin" property which will be used fro validation across the whole application
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}


	useEffect(() => {
		if (email !== '' && password !== ''){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}

	}, [email, password])

    return (

    	(user.id !== null) ?
    	<Navigate to="/courses" />
    	:
        <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" value={email} 
	                onChange={e => setEmail(e.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" value={password}
	                placeholder="Password"
	                onChange={e => setPassword(e.target.value)} 
	                required
                />
            </Form.Group>

 */