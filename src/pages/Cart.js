import {Row, Col, Button, Spinner, Card} from 'react-bootstrap';
import React from 'react'; ///<<


import Swal from 'sweetalert2';
import {useNavigate, Link} from 'react-router-dom';

//import UserContext from "../UserContext";
import {useEffect, useState, useCallback} from 'react'; //useContext removed useCallback removed to

import CartCard from '../components/CartCard';

//export const DataContext = React.createContext(); ///<<

export default function Cart() {




	const navigate = useNavigate();

	//const {user} = useContext(UserContext);

	const token = localStorage.getItem('token');

	const [cart, setCart]  = useState([]);

	//console.log("quantity sa cart state", cart); ///<< mga CartCards ang laman that's why react element yung typeof nya sa console
	//const [oldQuantity, setOldQuantity] = useState([]) ///<<
	//const [newQuantity, setNewQuantity]  = useState([oldQuantity]); ///<<

	const [isLoading, setIsLoading] = useState(false);
/*
	const onButtonClick = useCallback(() => {
		console.log("PRESSED A BUTTON WEEE");
	},[])*/

/*	const onButtonClick = () => {
		console.log("you pressed the butown")
	}*/


	//const [stringifiedCart, setStringifiedCart] = useState(JSON.stringify([]));
	const [subTotals, setSubTotals] = useState([]);

	
	//const [quantity, setQuantity] = useState([]);
	//const [productNames, setProductNames] = useState([]);
	//const [summaryItems, setSummaryItems] = useState([]);
	//console.log("productNames Array", productNames)
	//console.log("SubTotals Length", subTotals.length);
	const total = subTotals.reduce((x, y) => {
	  return x + y;
	}, 0);

	const fetchData = useCallback(async () => {


			setIsLoading(true);

			console.log("token CART.JS useeffect", token);

			await fetch(`${process.env.REACT_APP_API_URL}/users/cart`, {
				headers: {
				    Authorization: `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {

				console.log("data after GET", data);

				setCart(data.map(cart => {
				console.log("cart sa useEffect in Cart.js", cart);
					return <CartCard key={cart.productId} cart={cart} fetchData={fetchData} />  // onButtonClick={onButtonClick}
				}))

				setSubTotals(data.map(cart => {
				 	return cart.subTotal;
				}))

				//setStringifiedCart(data);

				setIsLoading(false);
			})
	},[token]);
	//console.log("Total Amount", totalAmount);

	useEffect(() => {
		fetchData();
	},[fetchData])

/// fetch the cart of a specific user from the database 
/*	useEffect(() => {

		//console.log("userId", user.id);	

		const token = localStorage.getItem('token');

		const fetchData = async () => {


			setIsLoading(true);

			console.log("token CART.JS useeffect", token);

			await fetch(`${process.env.REACT_APP_API_URL}/users/cart`, {
				headers: {
				    Authorization: `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {

				console.log("data after GET", data);

				setCart(data.map(cart => {
				console.log("cart sa useEffect in Cart.js", cart);
					return <CartCard key={cart.productId} cart={cart} />  // onButtonClick={onButtonClick}
				}))

				setSubTotals(data.map(cart => {
				 	return cart.subTotal;
				}))

				//setStringifiedCart(data);

				setIsLoading(false);
			})
		}

		fetchData();

	},[])*/ // << this is where the solution is located, which I dont know yet how to solve // ADD EVENT LISTENER?

	// either  i make this dependency work which would be godsend to me, or i need to make a way in order for child to give data to its parent so that I can make the summary work in the left side of the page.

	/// 8/4 maybe i have to put the onclick functions here on Cart.js which will be place on the mapped CartCard. BUT HOW? HAHAHAHAHHA



	// checkout button/function


	const checkout = (e) => {
	  e.preventDefault();


	  fetch(`${process.env.REACT_APP_API_URL}/users/cart/checkout`, {
	      method: 'POST',
	      headers: {
	          Authorization: `Bearer ${token}`
	      }
	  })
	  .then(res => res.json())
	  .then(data => {

	        console.log("data after POST CHECKOUT", data);

	        if(data !== null) {
	                     Swal.fire({
	                             title: "Checkout is Successful",
	                             icon: "success",
	                             text: "Track your order by clicking My Orders"
	                     })

	                     navigate("/");

	         } else {

	                       Swal.fire({
	                           title: "Something went wrong",
	                           icon: "error",
	                           text: "Please, try again."
	                       })
	                } 

	      })
	  }





	return(

	<Row>
	{isLoading ?
		<Col className="pt-4 mt-4" style={{textAlign: "center"}}>
		<Spinner
		  as="span"
		  animation="border"
		  style={{ width: "4rem", height: "4rem" }}
		  role="status"
		  aria-hidden="true"
		/>
		</Col>
		:

		(cart.length === 0) ?
		  <>

		  <Col className="pt-4 mb-5">
		      <h1>Cart is currently empty</h1>
		      <h2>Please check our Menu</h2>
		      <Button variant="primary" as={Link} to="/products" className="mb-5">Back to Products</Button>
		      <h1>Track your order/s here</h1>
		      <Button variant="warning" as={Link} to="/myOrders">My Orders</Button>
	      </Col>
	      </>
	     :
	     <>
	     <Col className="pt-2" xs={{span: 12, order: 1}} md={{span: 8, order: 1}}> 
	     	<h1>Order Summary </h1>
	     </Col>
	     <Col className="pt-2" xs={{span: 12, order: 3}} md={{span: 4, order: 2}}> 
	     	<h1>Total</h1>
	     </Col>
	     <Col xs={{span: 12, order: 2}} md={{span: 8, order: 3}} className="mb-5">
{/*	     	<DataContext.Provider value={[newQuantity, setNewQuantity]}>
	//cart state == the cartCards that were mapped*/}
	     	{cart}
{/*	     	</DataContext.Provider>*/}
	     	<Button variant="warning" as={Link} to="/products" className="mt-3">Add more Products</Button>

	     </Col>

	     <Col xs={{span: 12, order: 4}} md={{span: 4, order: 4}} className="mb-5" >
	     <Card style={{color: "white", textAlign: "right", backgroundColor: "#454545"}}>
	     <Card.Body>

	     <Row>
	     	<Col className="d-flex justify-content-start">
	     		<h6>Subtotal</h6>
	     	</Col>
	     	<Col className="d-flex justify-content-end">
	     		<h6>Php {total}</h6>
	     	</Col>
	     </Row>
	     <Row>
	     	<Col className="d-flex justify-content-start">
	     		<h6>Delivery Fee</h6>
	     	</Col>
	     	<Col className="d-flex justify-content-end">
	     		<h6>49</h6>
	     	</Col>
	     </Row>   	
	     <Row>
	     	<Col className="d-flex justify-content-start">
	     		<h4>Total Amount</h4>
	     	</Col>
	     	<Col className="d-flex justify-content-end">
	     		<h4>Php {total + 49}</h4>
	     	</Col>
	     </Row>
	     <Button onClick={(e) => checkout(e)} className="mt-3 ms-3">Checkout</Button>
	
	     </Card.Body>
	     </Card>
	     </Col>
	     </>
	 	
	}
	</Row>

	)
}