//import coursesData from '../data/coursesData.js';
import ProductCard from '../components/ProductCard.js';
import {Row, Col, Spinner} from 'react-bootstrap';

//import {TailSpin} from 'react-loader-spinner';

import { useState, useEffect } from 'react';

export default function Products(){

	const [products, setProducts]  = useState([]);
	const [isLoading, setIsLoading] = useState(false);


	useEffect(() => {

		const fetchData = async () => {

		setIsLoading(true);

		await fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data => {
		    console.log(data);

		    setProducts(data.map(product => {
		    	return <ProductCard key={product._id} product={product}/>
		    }))

			setIsLoading(false);   
		})
		}

		fetchData();
	}, []);

	return(
		<>
		<Row>
			<Col xs={12} style={{ textAlign: "center", color: "white"}}>
			<h1>Le Burger's Menu</h1>
			</Col>
		</Row>
		<Row className="mb-5">
			<Col xs={12} md={3}/>
			{isLoading ? 
			<Col className="pt-4" xs={12} md={6} style={{ textAlign: "center"}}>
			<Spinner animation="border" variant="dark" size="md" role="status"/>
			</Col>
			:
			<Col className="pt-4" xs={12} md={6}>
			{products}
			</Col>
			}
		</Row>
		</>

	)
}