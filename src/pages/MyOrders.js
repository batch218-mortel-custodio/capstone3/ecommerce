import { useState, useEffect } from 'react';

import {Row, Col, Button} from 'react-bootstrap';

import {Link} from 'react-router-dom';

import MyOrdersCard from '../components/MyOrdersCard';

export default function MyOrders() {

	const [orders, setOrders] = useState([]);
	// const [subTotals, setSubTotals] = useState([]);
	//console.log("SubTotals Array", subTotals);

	const token = localStorage.getItem('token');

	// const total = subTotals.reduce((x, y) => {
	//   return x + y;
	// }, 0);

	//console.log("total!!", total);


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/myOrders`, {
			headers: {
			    Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
		    console.log("myorders data", data);

		    setOrders(data.map(orders => {
		    	return <MyOrdersCard key={orders._id} orders={orders}/>
		    }))

		    // setSubTotals(data.map(cart => {
		    // 	return cart.subTotal;
		    // }))
		})
	}, [token]);

	return(
	<>
	<Row>
		<Col xs={1} md={2} />
		<Col xs={10} md={8}>
			<h1>You Have Ordered: </h1>
		</Col>
	</Row>
	<Row className="mb-2">
		<Col xs={1} md={2} />
		<Col xs={10} md={8}>
			{orders}
		</Col>
	</Row>
	<Row>
		<Col xs={1} md={2} />
		<Col xs={10} md={8} className="d-flex justify-content-end">
			<Button variant="primary" as={Link} to="/products" className="mb-5">Back to Products</Button>
		</Col>
	</Row>
	</>
	)
}