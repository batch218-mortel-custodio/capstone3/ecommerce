import Banner from '../components/Banner.js';
import { Link } from 'react-router-dom';

import {Row, Col, Button} from 'react-bootstrap';

import pic from '../burger-final.png';
import Highlights from '../components/Highlights.js';



export default function Home() {
	const data = {
		title: "THE BEST CHICKEN BURGER IN TOWN!",
		content: "Juicy, Fresh, and Made-to-Order",
		destination: "/login",
		label: "Try Now!"
	}


	return (
		<>
		<Row>
			<Col xs={12} md={4} style={{ textAlign: "right", color: "white" }}>
				<Banner data={data}/>
			</Col>
			<Col xs={12} md={8}>
				<img src={pic} width="100%" alt="burger"/>
			</Col>
		</Row>
		<Row style={{ textAlign: "center", color: "white" }} className="my-5">
			<h2>Le Burger's Other Best-Selling Burgers</h2>
		</Row>
		{/*style={{ border: "1px solid white" }}*/}
		<Row>
		<Highlights />
		</Row>
		<Row className="mb-5">
		<Col xs={12} md={{span: 6, offset: 3}} className="text-center">
			<Button variant="success" as={Link} to="/login">Order Now!</Button>
		</Col>
		</Row>
		</>
	)
}