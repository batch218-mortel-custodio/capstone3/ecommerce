
import {Navigate} from 'react-router-dom';

import UserContext from "../UserContext";

import {useEffect, useContext} from 'react';


export default function Logout() {
	// consume the UserContext  object and destructure it to accesss the user state and unsetUser function from context provider.
	const {unsetUser, setUser} = useContext(UserContext);

	unsetUser();

	useEffect(() =>{
		setUser({id: null});
	})

	return (

		<Navigate to="/" />
	)


}