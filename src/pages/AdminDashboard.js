import {useState, useEffect} from 'react';
import { Form, Button, Accordion, Row, Col } from 'react-bootstrap';

import ProductCardDashboard from '../components/ProductCardDashboard';



import Swal from 'sweetalert2';

import {useNavigate} from 'react-router-dom';


export default function AdminDashboard() {


	const [products, setProducts]  = useState([]);

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [initialInventory, setInitialInventory] = useState(0);

	const [editId, setEditId] = useState();
	const [editName, setEditName] = useState("");
	const [editDescription, setEditDescription] = useState("");
	const [editPrice, setEditPrice] = useState("");
	const [editInitialInventory, setEditInitialInventory] = useState("");



	const [isActive, setIsActive] = useState(false);
	const [isEditActive, setIsEditActive] = useState(false);
	const navigate = useNavigate();

	function addProduct(e) {

		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
	                    method: "POST",
	                    headers: {
	                        'Content-Type': 'application/json'
	                    },
	                    body: JSON.stringify({
		                        name: name,
		                        description: description,
		                        price: price,
		                        initialInventory: initialInventory
	                    })
	                })
	                .then(res => res.json())
	                .then(data => {
	                    console.log("data", data);

	                    if(data !== null) {
	                    // Clear input fields
		                    setName("");
		                    setDescription("");
		                    setPrice("");
		                    setInitialInventory("");

		                    Swal.fire({
		                            title: "New Product Added!",
		                            icon: "success",
		                            text: "Refresh this page to reflect the added product."
		                    })

		                    navigate("/dashboard");

	                    } else {

	                        Swal.fire({
	                            title: "Something went wrong",
	                            icon: "error",
	                            text: "Please, try again."
	                        })
	                    }
	                })


		};

	const editFinalProduct = (e) => {

		    e.preventDefault()

		    setEditId(document.querySelector("#edit-id").placeholder);

		    console.log("id - editFinalProduct", editId);
		    console.log("id - placeholder", document.querySelector("#edit-id").placeholder);
		    console.log("name - editFinalProduct", editName);
		    fetch(`${process.env.REACT_APP_API_URL}/products/update/${editId}`, {
		                      method: "PATCH",
		                      headers: {
		                          'Content-Type': 'application/json'
		                      },
		                      body: JSON.stringify({
		                            name: editName,
		                            description: editDescription,
		                            price: editPrice,
		                            initialInventory: editInitialInventory
		                      })
		                  })  

		          .then(res => res.json())
		          .then(data => {

		         if(data !== null) {
		         			console.log("updated data", data);
		         			  setEditId("");
		                      setEditName("");
		                      setEditDescription("");
		                      setEditPrice("");
		                      setEditInitialInventory("");

		                      Swal.fire({
		                              title: "Product is now Updated",
		                              icon: "success",
		                              text: "Refresh this page to reflect the changes."
		                      })

		                      navigate("/dashboard");

		          } else {

		                        Swal.fire({
		                            title: "Something went wrong",
		                            icon: "error",
		                            text: "Please, try again."
		                        })
		          } 
		  })
		}

		useEffect(() => {
			fetch(`${process.env.REACT_APP_API_URL}/products/all`)
			.then(res => res.json())
			.then(data => {
			   

		   		 setProducts(data.map(product => {
			    	//console.log(product);
			    	return <ProductCardDashboard key={product._id} product={product}/>
			    }))
			})
		}, []);

		useEffect(() => {


		       // Validation to enable the submit button when all fields are populated and both passwords match.
		        if(name !== '' && description !== '' && price !== 0 && initialInventory !== 0){
		            setIsActive(true);
		        } else {
		            setIsActive(false);
		        }

		    }, [name, description, price, initialInventory])

		useEffect(() => {


		       // Validation to enable the submit button when all fields are populated and both passwords match.
		        if(editName !== '' && editDescription !== '' && editPrice !== 0 && editInitialInventory !== 0){
		            setIsEditActive(true);
		        } else {
		            setIsEditActive(false);
		        }

		    }, [editName, editDescription, editPrice, editInitialInventory])

		// useEffect(() => {
		// 		console.log("useeffect is kinda working")
		// 		setEditId(document.querySelector("#edit-id").placeholder);
		// }, [])
		


/*		useEffect(() => {


		       // Validation to enable the submit button when all fields are populated and both passwords match.
		        if(document.querySelector("#edit-name").placeholder !== null){
		        	console.log(document.querySelector("#edit-id").value);
		            
		        } else {
		            setEditId("");
		        }

		    }, [])*/

		

		return(
			<>
			<Row>
			<Col xs={12} md={1}/>
			<Col xs={12} md={10} className="pt-4 mb-5">

			<Accordion>
			      <Accordion.Item eventKey="0">
			        <Accordion.Header>Add Product</Accordion.Header>
			        <Accordion.Body>
					        <Form onSubmit={(e) => addProduct(e)}>

					          <Form.Group className="mb-3" controlId="name">
					            <Form.Label>Product Name</Form.Label>
					            <Form.Control 
					                type="text"
					                value={name}
					                onChange={(e) => {setName(e.target.value)}}
					                placeholder="Enter the product name." 
					                required
					                />
					          </Form.Group>

					        
					          <Form.Group className="mb-3" controlId="description">
					            <Form.Label>Description</Form.Label>
					            <Form.Control 
					                type="text"
					                value={description}
					                onChange={(e) => {setDescription(e.target.value)}}
					                placeholder="Enter the product's description." />
					          </Form.Group>

					          <Form.Group className="mb-3" controlId="price">
					            <Form.Label>Price</Form.Label>
					            <Form.Control 
					                type="number"
					                value={price}
					                onChange={(e) => {setPrice(e.target.value)}}
					                placeholder="Enter price" />
					          </Form.Group>

					          <Form.Group className="mb-3" controlId="initialInventory">
					            <Form.Label>Initial Inventory</Form.Label>
					            <Form.Control 
					                type="number"
					                value={initialInventory}
					                onChange={(e) => {setInitialInventory(e.target.value)}}
					                placeholder="Enter initial inventory here" />
					          </Form.Group>

					          { isActive ?
					                    <Button variant="primary" type="submit" id="addProduct">
					                     Add Product
					                    </Button>
					                    :
					                    <Button variant="primary" type="submit" id="addProduct" disabled>
					                     Add Product
					                    </Button>
					          }
					         
					        </Form> 

			        </Accordion.Body>
			      </Accordion.Item>
			      <Accordion.Item eventKey="1">
			        <Accordion.Header>Retrieve/Edit Products</Accordion.Header>
			        <Accordion.Body>
			          <Row>
				          <Col xs={{order: 2}}>
				          	{products}
				          </Col>
				          <Col xs={{order: 1}}>
				          	<Form onSubmit={(e) => editFinalProduct(e)}>

				           	  <Form.Group controlId="edit-id">
				          	    <Form.Control type="text"
				         
				          	    hidden/>
				          	  </Form.Group>	
				          	  	
				          	  <Form.Group className="mb-3" controlId="edit-name">
				          	    <Form.Label>Product Name</Form.Label>
				          	    <Form.Control 
				          	        type="text"
				          	        value={editName}
				          	        onChange={(e) => {setEditName(e.target.value)}}
				          	         
				          	        required
				          	        />
				          	  </Form.Group>

				          	
				          	  <Form.Group className="mb-3" controlId="edit-description">
				          	    <Form.Label>Description</Form.Label>
				          	    <Form.Control 
				          	        type="text"
				          	        value={editDescription}
				          	        onChange={(e) => {setEditDescription(e.target.value)}}
				          	         />
				          	  </Form.Group>

				          	  <Form.Group className="mb-3" controlId="edit-price">
				          	    <Form.Label>Price</Form.Label>
				          	    <Form.Control 
				          	        type="number"
				          	        value={editPrice}
				          	        onChange={(e) => {setEditPrice(e.target.value)}}
				          	         />
				          	  </Form.Group>

				          	  <Form.Group className="mb-3" controlId="edit-initialInventory">
				          	    <Form.Label>Initial Inventory</Form.Label>
				          	    <Form.Control 
				          	        type="number"
				          	        value={editInitialInventory}
				          	        onChange={(e) => {setEditInitialInventory(e.target.value)}}
				          	   		 />
				          	  </Form.Group>

				          	  { isEditActive ?
				          	            <Button variant="primary" type="submit" id="editProduct">
				          	             Edit Product
				          	            </Button>
				          	            :
				          	            <Button variant="primary" type="submit" id="editProduct" disabled>
				          	             Edit Product
				          	            </Button>
				          	  }
				          	 
				          	</Form> 
				          </Col>
			          </Row>
			        </Accordion.Body>
			      </Accordion.Item>
			    </Accordion>
			</Col>
			</Row>
		</>

			




		)



}