
import {useState, useEffect, useContext} from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';

import Swal from 'sweetalert2';
//import Loader from 'react-loader-spinner';

import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from "../UserContext";

export default function Register() {

	const [firstName, setFirstName] = useState(""); 
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	const [isActive, setIsActive] = useState(false);

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const [isLoading, setIsLoading] = useState(false);




	const registerUser = async (e) => {
	        // prevents page redirection via form submission
	        e.preventDefault()


	        setIsLoading(true);

	        // S55 ACTIVITY
	        await fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
	            method: "POST",
	            headers: {
	                'Content-Type': 'application/json'
	            },
	            body: JSON.stringify({
	                email: email
	            })
	        })
	        .then(res => res.json())
	        .then(data => {
	            console.log(data)

	            if (data === true) {

	                Swal.fire({
	                    title: "Duplicate Email Found",
	                    icon: "error",
	                    text: "Kindly provide another email to complete registration."
	                })

	                setIsLoading(false);

	            } else {

	                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
	                    method: "POST",
	                    headers: {
	                        'Content-Type': 'application/json'
	                    },
	                    body: JSON.stringify({
	                        firstName: firstName,
	                        lastName: lastName,
	                        email: email,
	                        mobileNo: mobileNo,
	                        password: password1
	                    })
	                })
	                .then(res => res.json())
	                .then(data => {
	                    console.log("data", data);

	                    if(data !== null) {
	                    // Clear input fields
		                    setFirstName("");
		                    setLastName("")
		                    setEmail("");
		                    setMobileNo("");
		                    setPassword1("");
		                    setPassword2("");

		                    Swal.fire({
		                            title: "Registration Successful",
		                            icon: "success",
		                            text: "Welcome to Le Burger!"
		                    })


		                    setIsLoading(false);

		                    navigate("/login");

	                    } else {

	                        Swal.fire({
	                            title: "Something went wrong",
	                            icon: "error",
	                            text: "Please, try again."
	                        })

	                        setIsLoading(false);
	                    }
	                })
	            }
	        })

	        //alert('Thank you for registering!');
	    }

	    // S55 ACTIVITY
	    useEffect(() => {
	        /*
	        MINI ACTIVITY
	        Create a validation to enable the submit button when all fields are populated and both passwords match.
	        7:45 PM
	        */

	            // Validation to enable the submit button when all fields are populated and both passwords match.
	            if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
	                setIsActive(true);
	            } else {
	                setIsActive(false);
	            }

	        }, [email, password1, password2])


	    return (
	        (user.id !== null) ? // S54 ACTIVITY
	        <Navigate to ="/courses" /> // S54 ACTIVITY
	        : // S54 ACTIVITY
	        <Row>
	        <Col xs={2} md={4}/>
	        <Col xs={8} md={4} className="pt-4 mb-5">
	        <Form onSubmit={(e) => registerUser(e)}>

	            {/*S55 ACTIVITY*/}
	            <Form.Group className="mb-3" controlId="firstName">
	            <Form.Label>First Name</Form.Label>
	            <Form.Control 
	                type="text"
	                value={firstName}
	                onChange={(e) => {setFirstName(e.target.value)}}
	                placeholder="Enter your First Name" 
	                required
	                />
	          </Form.Group>

	          {/*S55 ACTIVITY*/}
	          <Form.Group className="mb-3" controlId="lastName">
	            <Form.Label>Last Name</Form.Label>
	            <Form.Control 
	                type="text"
	                value={lastName}
	                onChange={(e) => {setLastName(e.target.value)}}
	                placeholder="Enter your Last Name" />
	          </Form.Group>

	          <Form.Group className="mb-3" controlId="userEmail">
	            <Form.Label>Email address</Form.Label>
	            <Form.Control 
	                type="email"
	                value={email}
	                onChange={(e) => {setEmail(e.target.value)}}
	                placeholder="Enter email" />
	            <Form.Text className="text-muted">
	              We'll never share your email with anyone else.
	            </Form.Text>
	          </Form.Group>

	          {/*S55 ACTIVITY*/}
	          <Form.Group className="mb-3" controlId="mobileNo">
	            <Form.Label>Mobile Number</Form.Label>
	            <Form.Control 
	                type="text"
	                value={mobileNo}
	                onChange={(e) => {setMobileNo(e.target.value)}}
	                placeholder="0999999999" />
	          </Form.Group>

	          <Form.Group className="mb-3" controlId="password1">
	            <Form.Label>Password</Form.Label>
	            <Form.Control 
	                type="password" 
	                value={password1}
	                onChange={(e) => {setPassword1(e.target.value)}}
	                placeholder="Enter Your Password" />
	          </Form.Group>

	          <Form.Group className="mb-3" controlId="password2">
	            <Form.Label>Verify Password</Form.Label>
	            <Form.Control 
	                type="password" 
	                value={password2}
	                onChange={(e) => {setPassword2(e.target.value)}}
	                placeholder="Verify Your Password" />
	          </Form.Group>
	          { isActive ?
	                    <Button variant="primary" type="submit" id="submitBtn">
	                    {isLoading ? "Loading" : "Submit" }
	                    </Button>
	                    :
	                    <Button variant="primary" type="submit" id="submitBtn" disabled>
	                    {isLoading ? "Loading" : "Submit" }
	                    </Button>
	          }	
	         
	        </Form>
	       	</Col>
	       	</Row>
	    )

	}
