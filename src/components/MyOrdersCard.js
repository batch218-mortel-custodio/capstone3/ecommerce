import {Row, Col, Card, Table} from 'react-bootstrap';

import {useState ,useEffect} from 'react';

export default function MyOrdersCard({orders}) {


	const {_id, totalAmount, purchasedOn, products} = orders;

	const [listProducts, setListProducts] = useState([]);


	useEffect(() => {

				setListProducts(products.map(product => {;
					return <tr key={product.productId}><td>{product.productName}</td> <td>{product.price}</td> <td>{product.quantity}x</td><td> {product.subTotal}</td> </tr>
				}))

	},[products])

	return (
	  <Card style={{ width: '100%' }} className="mb-1">

	    <Card.Body>
	      <Row>
	        <Col>
	          <Card.Title>Order No. {_id}</Card.Title>
	        </Col>
	      </Row>
	      <Row>
	        <Col>
	          <Table striped bordered hover variant="info">
	          <thead><tr><th>Items Ordered:</th><th>Price</th><th>Quantity</th><th>Subtotal</th></tr></thead>
	          <tbody>
	          {listProducts}
	          <tr><td></td><td></td><td>Total Amount:</td> <td>PhP {totalAmount}</td></tr>
	          </tbody>
	          </Table>
	        </Col>
	      </Row>
	      <Row>
	        <Col>
	       	 <Card.Text>Purchased On: {purchasedOn}</Card.Text>
	        </Col>
	      </Row>
	    </Card.Body>
	  </Card>
	);

}