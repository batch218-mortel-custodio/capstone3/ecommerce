import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';

import UserContext from "../UserContext";

//import Swal from 'sweetalert2';

import {useParams, Link} from "react-router-dom"; //useNavigate removed

export default function ProductView() {

	const {user} = useContext(UserContext);

	//const navigate = useNavigate();

	//useParams - type of hook that allows us to retrieve the courseId that can be passed via url params
	const {courseId} = useParams();
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

/*
	const enroll = (courseId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`,{
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				 Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data === true){
				Swal.fire({
				  title: "Successfully enrolled",
				  icon: "success",
				  text: "You have successfully enrolled for this course."
				})

				navigate("/courses");
			} 
			else {
				 Swal.fire({
				  title: "Something went wrong",
				  icon: "error",
				  text: "Please try again."
				})
			}

		})
	}
*/
	useEffect(() => {

		console.log(courseId);
		fetch(`${process.env.REACT_APP_API_URL}/products/${courseId}`)
		.then(res => res.json())
		.then(data => {

		

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		})

	}, [courseId])
	

	return (

		<Container>
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card>
					      <Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					        
       						{
						        (user.id !== null) ?
						        	<Button variant="primary" as={Link} to="/products">Back to Products</Button>
						        		:
						        	<Button className="btn btn-danger" as={Link} to="/login" >Log in</Button>
					        }

					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}