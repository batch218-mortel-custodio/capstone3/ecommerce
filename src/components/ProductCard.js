//import {useState, useEffect} from 'react';

import Swal from 'sweetalert2';
import {useNavigate} from 'react-router-dom';

import {Button, Card, Row, Col, InputGroup, Spinner} from 'react-bootstrap';
import { useState, useEffect } from 'react';

import {Link} from "react-router-dom";

import PropTypes from 'prop-types';

export default function ProductCard({product}) {

  const navigate = useNavigate();

  const {name, description, price, _id} = product;

  const token = localStorage.getItem('token');

  // const [quantity, setQuantity] =  useState(0);
  
  // const increaseQuantityByOne = () => {
  //   setQuantity(quantity + 1);
  // };
  // const decreaseQuantityByOne = () => {
  //   if (quantity > 0){
  //   setQuantity(quantity - 1);
  //   }
  // };
  const [isLoading, setIsLoading] = useState(false);
  const [isProductInCart, setIsProductInCart] = useState(false);
  const [isProductLoading, setIsProductLoading] = useState(false);

  const handleClick = (e) => {
      e.currentTarget.disabled = true;
      console.log(e);
      console.log('button clicked');
    };


///////////////////////////////////////////////////////////////////////////////////
  useEffect(() => {

    //console.log("userId", user.id); 

    const token = localStorage.getItem('token');

    const fetchData = async () => {

      setIsProductLoading(true);

      await fetch(`${process.env.REACT_APP_API_URL}/users/cart`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => {

       // console.log("data after GET", data);

        const filteredResult = data.find((e) => e.productId === _id);

        if (filteredResult !== undefined){
          setIsProductInCart(true);
        }
        setIsProductLoading(false);
      })
    }

    fetchData();

  }, [_id])

//////////////////////////////////////////////////////////////////////////////////



  const addToCart = async (e) => {
    e.preventDefault();

    setIsLoading(true);

    await fetch(`${process.env.REACT_APP_API_URL}/users/cart/addtocart`, {
        method: 'POST', 
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
        },
        body: JSON.stringify({
            productId: _id,
            quantity: 1
        })
    })
    .then(res => res.json())
    .then(data => {

          console.log("data after POST", data);

          if(data !== null) {
                       Swal.fire({
                               title: "Product is now added to Cart",
                               icon: "success",
                               text: "Refresh this page to reflect the changes."
                       })

                       navigate("/cart");

                       setIsLoading(false);

           } else {

                         Swal.fire({
                             title: "Something went wrong",
                             icon: "error",
                             text: "Please, try again."
                         })

                         setIsLoading(false);
                  } 

        })
    }

  

  return (
    <Card className="mb-1">

      <Card.Body>
        <Row>
        <Col xs={4} md={6}>

        </Col>
        {isProductLoading ?
        <Col xs={8} md={6} style={{ alignItems: "center"}}>
        <Spinner animation="border" variant="dark" size="md" role="status"/>
        </Col>
        :
        <Col xs={8} md={6} style={{ textAlign: "left"}}>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle className="mb-2">Description: {description}</Card.Subtitle>
            <Card.Subtitle className="mb-2">Price: PhP {price}</Card.Subtitle>
            <InputGroup className="mb-2" >
            {/*<Button variant="outline-dark" onClick={decreaseQuantityByOne}>-</Button>
            <p className="ms-2 me-3">Qty: {quantity}</p>
            <Button variant="outline-dark" onClick={increaseQuantityByOne}>+</Button>*/}
            </InputGroup>
            {isProductInCart ?
            <Button variant="primary" className="me-2" as={Link} to={`/cart`}>
            View Cart
            </Button>
            :
            <Button variant="warning" className="me-2" onClick={(e) => {addToCart(e); handleClick(e)}}>
                {isLoading ?         
                <Spinner
                  as="span"
                  animation="border"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                />
                :
                "Add To Cart"}
            </Button>
            }
            <Button variant="success" as={Link} to={`/products/${_id}`} >Details</Button>
        </Col>
      }
        </Row>
      </Card.Body>
    </Card>
  );
}



ProductCard.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}