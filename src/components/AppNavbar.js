import '../App.css';

import Button from 'react-bootstrap/Button';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import {Link, NavLink} from 'react-router-dom';
import {useContext} from 'react';


import Logo from '../leburger-final.png';

import UserContext from "../UserContext";

export default function AppNavbar() {


const {user} = useContext(UserContext);

  
  return (
    <Navbar className="navbar-layout" expand="md">
 
    <Navbar.Brand as={Link} to="/"><img src={Logo} alt="logo" width="180"/></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end pe-5 ps-5">
          <Nav>

            <Nav.Link as={NavLink} to="/">Home</Nav.Link>

            {(user.id !== null) ?
                (user.isAdmin === false) ?
                  <>
                  <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
                  <Nav.Link as={NavLink} to="/cart">Cart</Nav.Link>
                  <Nav.Link as={NavLink} to="/myorders">My Orders</Nav.Link>
                  <Nav.Link>Hello, {user.firstName}!</Nav.Link>
                  <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                  </>
                  :
                  <>
                  <Nav.Link as={NavLink} to="/dashboard">Dashboard</Nav.Link>
                  <Nav.Link>Hello, Admin!</Nav.Link>
                  <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                  </>
                
                :
                <>
                <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                <Button className="order-button" as={NavLink} to="/login">Order Now</Button>
                </>
            }

          </Nav>
        </Navbar.Collapse>
  
    </Navbar>
  );

}

