import { Row, Col } from "react-bootstrap";
import burger1 from '../cheesyburger.png';
import burger2 from '../doubleburger.png';



export default function Highlights() {
	return (

		<Row>
			<Col xs={12} md={2} />
			<Col xs={12} md={4}>
				<img src={burger1} width="100%" alt="burger1"/>
			</Col>
			<Col xs={12} md={4}>
				<img src={burger2} width="93%" alt="burger2"/>
			</Col>
		</Row>
	)
}