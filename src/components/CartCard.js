import {Button, Card, Row, Col, InputGroup} from 'react-bootstrap';


import {useState ,useEffect} from 'react';

// import {DataContext} from "../pages/Cart"; ///<<


export default function CartCard({cart, fetchData}) { //,quantity, onButtonClick

  const {productId, productName, quantity, price, subTotal} = cart;
 // const {oldQuantity} = quantity 


  const token = localStorage.getItem('token');
  //const [isLoading, setIsLoading] = useState(false);

  //quantity is STUCK at the initial render / fetch from database. quantity doesn't follow the newQuantity unless REFRESHED.

  const [newQuantity, setNewQuantity]  = useState(quantity);
  const [newSubTotal, setNewSubTotal]  = useState(subTotal);

  // const newQuantityContext = c

  const increase = async() => {
    await increaseQuantityByOne()
    await fetchData()
  }

  const decrease = async() => {
    await decreaseQuantityByOne()
    await fetchData()
  }

  const remove = async() => {
    await removeFromCart()
    await fetchData()
  }

  const increaseQuantityByOne = () => {

    setNewQuantity(newQuantity + 1);
    setNewSubTotal(newSubTotal + price);

    //onButtonClick()
  };


  const decreaseQuantityByOne = () => {
    if (quantity > 0){
    setNewQuantity(newQuantity - 1);
    setNewSubTotal(newSubTotal - price);
    //onButtonClick()
    }
  };

useEffect(() => {


  //console.log("useEffect newQuantity lets go", newQuantity)

  
  const changeQuantity = async () => {

    //setIsLoading(true);

    await fetch(`${process.env.REACT_APP_API_URL}/users/cart/addtocart`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
        },
        body: JSON.stringify({
            productId: productId,
            quantity: newQuantity
        })
    })
  }

  changeQuantity();
 // setIsLoading(false);


},[newQuantity, productId, token])



  //console.log("useEffect newQuantity lets go", newQuantity)

  
  const removeFromCart = async () => {

    //setIsLoading(true);

    await fetch(`${process.env.REACT_APP_API_URL}/users/cart/removefromcart`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
        },
        body: JSON.stringify({
            productId: productId,
            quantity: quantity
        })
    })
  }






  // useEffect(() => {

    
  // },[newQuantity, newSubTotal, changeQuantity]);
  

  // useEffect(() => {
  //   fetch(`${process.env.REACT_APP_API_URL}/users/cart/addtocart`, {
  //       method: 'POST',
  //       headers: {
  //           'Content-Type': 'application/json',
  //           Authorization: `Bearer ${token}`
  //       },
  //       body: JSON.stringify({
  //           productId: productId,
  //           quantity: newQuantity
  //       })
  //   })
  //   .then(res => res.json())
  //   .then(data => {
  //       console.log("useEffect usage", data);

  //   })
  // });

  //console.log("productId state", productId);
  console.log("quantity state", quantity);
  console.log("newQuantity state", newQuantity);
  //console.log("newSubTotal state", newSubTotal);
  //console.log("price state", price);

  return (
    <Card>

      <Card.Body>
        <Row>
          <Col xs={12} md={4}>
            <Card.Title>{productName}</Card.Title>
            <Card.Text>Price: {price}</Card.Text>
          </Col>
          <Col xs={7} md={4} className="mt-2">

            <InputGroup>
            {quantity === 1 ?
            <Button variant="dark" onClick={decrease} disabled>-</Button>
            :
            <Button variant="outline-dark" onClick={decrease}>-</Button>
            }
            <p className="ms-2 me-3">{newQuantity}x</p>
            <Button variant="outline-dark" onClick={increase}>+</Button>
            
            </InputGroup>
            

          </Col>
          <Col xs={5} md={2} style={{textAlign: "right"}}>
            <Card.Text>Php {newSubTotal}</Card.Text>
          </Col>
          <Col xs={5} md={2} style={{textAlign: "right"}}>
            <Button variant="outline-danger" onClick={remove}>X</Button>
          </Col>
        </Row>  
      </Card.Body>
    </Card>
  );
}
