import Swal from 'sweetalert2';

import {useNavigate} from 'react-router-dom';

import {Card, Row, Col, Button} from 'react-bootstrap';

import logo from "../logo.png";

import PropTypes from 'prop-types';

export default function ProductCardDashboard({product}) {

  const {name, description, price, isActive, initialInventory, _id} = product;

  const navigate = useNavigate();

    const editProduct = () => {

      console.log("edit _id", _id);

      document.querySelector("#edit-id").value = "";
      document.querySelector("#edit-name").value = "";
      document.querySelector("#edit-description").value = "";
      document.querySelector("#edit-price").value = "";
      document.querySelector("#edit-initialInventory").value = "";

                            document.querySelector("#edit-id").placeholder = _id;
                            document.querySelector("#edit-name").placeholder = name;
                            document.querySelector("#edit-description").placeholder = description;
                            document.querySelector("#edit-price").placeholder = price;
                            document.querySelector("#edit-initialInventory").placeholder = initialInventory;
    }


  



    const deactivateProduct = (_id) => {

      console.log("id", _id);
      fetch(`${process.env.REACT_APP_API_URL}/products/archive/${_id}`, {
                        method: "PATCH",
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                              isActive: false
                        })
                    })  

            .then(res => res.json())
            .then(data => {

           if(data !== null) {
                        Swal.fire({
                                title: "Product is now Archived",
                                icon: "success",
                                text: "Refresh this page to reflect the changes."
                        })

                        navigate("/dashboard");

                      } else {

                          Swal.fire({
                              title: "Something went wrong",
                              icon: "error",
                              text: "Please, try again."
                          })
                      } 
    })
}


    const activateProduct = (_id) => {

      console.log("id", _id);
      fetch(`${process.env.REACT_APP_API_URL}/products/activate/${_id}`, {
                        method: "PATCH",
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                              isActive: true
                        })
                    })  

            .then(res => res.json())
            .then(data => {

           if(data !== null) {
                        Swal.fire({
                                title: "Product is now Activated!",
                                icon: "success",
                                text: "Refresh this page to reflect the changes."
                        })

                        navigate("/dashboard");

                      } else {

                          Swal.fire({
                              title: "Something went wrong",
                              icon: "error",
                              text: "Please, try again."
                          })
                      } 
    })
}


  return (
    <Card>
      <Row>
        <Col>
          <Card.Img variant="top" src={logo} />
        </Col>
        <Col>
          <Card.Body>
                  <Card.Title>{name}</Card.Title>
                  <Card.Text>Description:   {description}</Card.Text>
                  <Card.Text>Price:   PhP {price}</Card.Text>
                  {(isActive === true) ?
                  <Card.Text>isProductActive: Yes</Card.Text>
                  :
                  <Card.Text>isProductActive: No</Card.Text>
                  }
          </Card.Body>
        </Col>
      </Row>
      <Row className="text-center">
        <Col>
          <Button variant="outline-dark" onClick={editProduct}>Edit Product Details</Button>
        </Col>
        {(isActive === false) ?
        <Col>
         <Button variant="outline-success" onClick={() => activateProduct(_id)}>Reactivate Product</Button>
        </Col>
        :
        <Col>
          <Button variant="outline-danger" onClick={() => deactivateProduct(_id)}>Deactivate Product</Button>
        </Col>
        }
      </Row>
    </Card>
  );
}



ProductCardDashboard.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  })
}